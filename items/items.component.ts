import { Component, OnInit } from '@angular/core';

export class User {
  name: string;
  constructor() {
    this.name = 'mike';
  }
}

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  user: User;

  constructor() {
    this.user = new User();
  }

  ngOnInit() {
  }

}
