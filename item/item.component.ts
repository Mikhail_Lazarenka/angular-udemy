import {Component, Input, OnInit} from '@angular/core';
import {User} from '../items/items.component';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input()
  user: User;


  input: any;
  color: any;

  constructor() { }

  ngOnInit() {
  }

  onInput(event: Event){
    console.log(event);
    this.color = (<HTMLInputElement>event.target).value;
  }

}
